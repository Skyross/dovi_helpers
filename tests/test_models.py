import json
from typing import List

import pytest
from pydantic import ValidationError, parse_raw_as

from dovi_helpers.models.info import MediaInfoData


def test_info_data_parsing(info_data_sample) -> None:
    data = json.loads(json.dumps(MediaInfoData.parse_raw(info_data_sample).dict()))
    assert data == {
        "media": {
            "ref": "Black Panter.mkv",
            "tracks": [
                {
                    "type": "General",
                    "unique_id": "8623896046047272694806526875683546263",
                },
                {
                    "codec_id": "V_MPEGH/ISO/HEVC",
                    "hdr": {
                        "compatibility": "Blu-ray / HDR10",
                        "format": "Dolby Vision / SMPTE ST 2086",
                        "level": "06 / ",
                        "profile": "dvhe.07 / ",
                        "settings": "BL+EL+RPU / ",
                        "version": "1.0 / ",
                    },
                    "id": 1,
                    "type": "Video",
                    "unique_id": "1",
                },
                {"codec_id": "A_AC3", "id": 2, "type": "Audio", "unique_id": "2"},
                {"codec_id": "A_TRUEHD", "id": 3, "type": "Audio", "unique_id": "3"},
                {"codec_id": "A_AC3", "id": 4, "type": "Audio", "unique_id": "4"},
                {"codec_id": "A_AC3", "id": 5, "type": "Audio", "unique_id": "5"},
                {"codec_id": "A_AC3", "id": 6, "type": "Audio", "unique_id": "6"},
                {"codec_id": "S_HDMV/PGS", "id": 7, "type": "Text", "unique_id": "7"},
                {"codec_id": "S_HDMV/PGS", "id": 9, "type": "Text", "unique_id": "9"},
                {"codec_id": "S_HDMV/PGS", "id": 11, "type": "Text", "unique_id": "11"},
                {"codec_id": "S_HDMV/PGS", "id": 13, "type": "Text", "unique_id": "13"},
                {"codec_id": "S_HDMV/PGS", "id": 15, "type": "Text", "unique_id": "15"},
                {"type": "Menu"},
            ],
        },
        "meta": {"version": "22.06"},
    }


def test_info_data_parsing_list(info_data_sample) -> None:
    with pytest.raises(ValidationError) as err:
        parse_raw_as(List[MediaInfoData], info_data_sample)
    assert "value is not a valid list" in str(err)

    raw_data_list = "[" + ",".join([info_data_sample] * 3) + "]"
    mediainfo_data = parse_raw_as(List[MediaInfoData], raw_data_list)
    assert len(mediainfo_data) == 3
    assert isinstance(mediainfo_data[0], MediaInfoData)
